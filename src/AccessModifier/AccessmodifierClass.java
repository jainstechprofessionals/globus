package AccessModifier;

public class AccessmodifierClass {

	public void publicMethod() {
		System.out.println("in publicMethod");
	}

	private void privateMethod() {
		
		System.out.println("in privateMethod");
	}

	protected void protectedMethod() {
		System.out.println("in protectedMethod");
	}

	void defaultMethod() {
		System.out.println("in defaultMethod");
	}

}
