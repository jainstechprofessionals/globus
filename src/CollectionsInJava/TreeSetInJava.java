package CollectionsInJava;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class TreeSetInJava {

	public static void main(String[] args) {
		TreeSet<String> st = new TreeSet<String>();
		
		st.add("rounak");
		st.add("vikas");
		st.add("sakshee");
		st.add("julee");
		st.add("Altaf");
//		st.add(null);
		st.add("rounak");
		st.add("rounak jain");

		
//		for(String i :st) {
//			System.out.println(i);
//		}		
		Iterator<String> itr = st.iterator();		
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
	}

}
