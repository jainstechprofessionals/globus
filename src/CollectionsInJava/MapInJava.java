package CollectionsInJava;

import java.util.HashMap;
import java.util.Map;

public class MapInJava {

	public static void main(String[] args) {	

		Map<Integer,String> hs = new HashMap<Integer, String>();
		
		hs.put(101, "rounak");
		hs.put(102,"vikas");
		hs.put(103, "shilpi");
		hs.put(104, "nitin");
		hs.put(105, "mehak");
	
		hs.put(101, "julee");
		hs.put(null, "asdfsadf");
		
//	System.out.println(hs.containsKey(101));
		
		for(Map.Entry m: hs.entrySet()) {
			System.out.println(m.getKey() + "   "+ m.getValue());
		}
		
	
		
	}

}
