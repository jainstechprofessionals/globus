package CollectionsInJava;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetInJava {
	public static void main(String[] args) {
		Set<String> st = new HashSet<String>();
		st.add("rounak");
		st.add("vikas");
		st.add("sakshee");
		st.add("julee");
		st.add("Altaf");
		st.add(null);
		st.add("rounak ");
		st.add("rounak jain");

		
//		for(String i :st) {
//			System.out.println(i);
//		}		
		Iterator<String> itr = st.iterator();		
		while(itr.hasNext()) {
			System.out.println(itr.next());
		}
				
	}

}
