package CollectionsInJava;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;

public class TreeMapInJava {

	public static void main(String[] args) {
		TreeMap<Integer, String> hs = new TreeMap<Integer, String>();

		hs.put(101, "rounak");
		hs.put(102, "vikas");
		hs.put(103, "shilpi");
		hs.put(104, "nitin");
		hs.put(105, "mehak");

		hs.put(101, "julee");
		hs.put(109, null);
		hs.put(100, null);

//	System.out.println(hs.containsKey(101));

		for (Map.Entry m : hs.entrySet()) {
			System.out.println(m.getKey() + "   " + m.getValue());
		}


	}

}
