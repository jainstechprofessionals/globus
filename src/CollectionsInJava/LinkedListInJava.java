package CollectionsInJava;

import java.util.LinkedList;
import java.util.List;

public class LinkedListInJava {

	public static void main(String[] args) {
		
		List<String> lst = new LinkedList<String>();
		
		lst.add("rounak");
		lst.add("shakshee");
		lst.add("altaf");
		lst.add("shilpi");

//		System.out.println(lst.size());	
//		System.out.println(lst);		
//		System.out.println(lst.get(2));	

//		lst.remove(1);
		lst.add(1, "vikas");

//		boolean b = lst.contains("rounak");
//		System.out.println(b);

		for (int i = 0; i < lst.size(); i++) {
			System.out.println(lst.get(i));
		}
		
		

	}

}
