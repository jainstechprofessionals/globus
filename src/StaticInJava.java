
public class StaticInJava {
	
	int rollNo;  // instance variable (Global)
	String name;
	static String college="lnct";
	final String s = "asfd";
	
	StaticInJava(int a , String n){
		rollNo = a;
		name = n;	
	}	
	public void show() {

		System.out.println(rollNo);
		System.out.println(name);
		System.out.println(college);
	}	
	public static void display() {
		System.out.println("in display");
	}
	
	public static void main(String[] args) {
		
//		System.out.println(StaticInJava.college);		
		StaticInJava obj = new StaticInJava(101,"harsh");		
		StaticInJava roun = new StaticInJava(102,"Sagar");

//		System.out.println(obj.rollNo); //101
//		
//		System.out.println(StaticInJava.college);
		obj.name= "rounak";
		
		StaticInJava.college="oriental";
		obj.show();
		
		roun.show();
		
		
		StaticInJava.display();
		
	}

}
