
public class StringClassInJava {

	public static void main(String[] args) {
		String str = "rounak";
		
		
		char ch [] = {'r','o','u','n','a','k'};
		
		String str1 = new String(ch);
		
//		System.out.println(str1);
		
		
//		1. By String literal
		
		String s1 = "shilpi";
		String s2 = "shilpi";
		
		
	
		if(s1.equals(s2)) {
			System.out.println("equal");
		}else {
			System.out.println("not equal");
		}
		
		
//		2. By new Keyword
		String s3 = new String ("shilpi");
		
		String s4 = new String ("SHILPI");
 
		if(s3.equalsIgnoreCase(s4)) {
			System.out.println("equal");
		}else {
			System.out.println("not equal");
		}
		
	}

}
