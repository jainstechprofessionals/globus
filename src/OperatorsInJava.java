
public class OperatorsInJava {

	public static void main(String[] args) {

		// Arithmetic Operators
		// 1. Multiplicative (*, / ,%)
		// 2. Additive ( +,-)

//		int a = 21;
//		int b = 2;

//		System.out.println(a + b);
//
//		System.out.println(a - b);
//
//		System.out.println(a * b);
//
//		System.out.println(a / b);
//
//		System.out.println(a % b);
		
		
//		Relational Operator
//		1. Comparison ( < , > , <= ,>=)
//		2. Equality (==,!=)
		
		
//		System.out.println(a>b);  
//		System.out.println(a<b);
//		System.out.println(a>=b);
//		System.out.println(a<=b);
//		
//		System.out.println(a==b);
//
//		System.out.println(a!=b);
		
	
		// Assignment Operator ( = , +=, -= , *=, /=)
//		a=a+1;
//		a+=1;
//		System.out.println(a);
		
		// Unary Operator
		
		// Postfix exp ++ , exp --
		// Prefix   ++exp ,--exp
		
		int a=34 , temp=0;
		
//		temp = a++   + a++;   //  temp = 34 + 35;
		
//		temp = ++a - a--;  // temp = 35 - 35;
		
//		temp = a++ + ++a + --a - --a + a++;   // temp = 34 + 36 + 35 - 34 + 34 ;
		
//		temp = ++a - --a - --a - --a;   // temp = 35 - 34 - 33 - 32;
		
//		temp = a++ + ++a ;  // temp = 34 + 36;
		
//		temp  = a-- - --a - a--; // temp = 34 - 32 - 32;
		
	//	temp = --a - --a + --a - ++a + a++ - a--; // temp = 33 - 32 +31 - 32 +32 - 33 
		
//	temp = a-- - ++a;  // temp = 34 - 34
		
//		temp = a++ + a++ - --a - a-- ;    //temp = 34 + 35 - 35 - 35
		
//        temp = --a + ++a - --a + a--;  // 33 + 34 - 33 + 33; 
		
//		temp = a-- - a-- - --a - --a - ++a + ++a ;  // temp = 34 - 33 - 31 - 30 - 31 + 32;
		
//		temp  = --a - ++a - a++ ; // 33 - 34 - 34//
		
		
//		temp = a -- - --a - --a - a++; // temp = 34 - 32 - 31 - 31 // 
		
		temp = a++ - ++a - --a + ++a ; // 34- 36 - 35 + 36;

		System.out.println(temp); //  69     69   72   71     69
		System.out.println(a);   //   36     36   36   36   36
		
		
//		
//		temp = ++a - --a + a++ - --a - --a - --a ;
		
		
		
		
		
		
	}

}
