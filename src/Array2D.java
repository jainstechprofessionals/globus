
public class Array2D {

	public static void main(String[] args) {

		int arr[][] = new int[3][4];

		int temp = 10;

		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				arr[i][j] = temp;
				temp++;
			}
		}
		
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {

				System.out.println("value of arr[" +i+"]["+j+"] is : " +arr[i][j]);
			}
		}
	}

}
