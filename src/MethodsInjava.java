
public class MethodsInjava {

	public void show() {
		System.out.println("in show");
	}

	public void display(String name) {
		System.out.println(name);
	}

	// private
	// int
	// harsh
	// int , String
	private int harsh(int i, String s) {
		return 0;
	}
	// Protected
	// String
	// Sakshee
	// String , int , float

	protected String sakshee(String str, int i, float j) {
		return "asdf";
	}

//	public
//	int 
	// Sagar
	public int sagar() {
		System.out.println("asdf");
		return 8;
	}

	public void evenOdd(int i) {

		if (i % 2 == 0) {
			System.out.println("even number");
		} else {
			System.out.println("odd number");
		}

	}
	
	public String calculateAge(int i) {
		if(i<18) {
			return "kids";
		}else {
			return "adults";
		}
	}

	public void channels(String s) {
		if(s.equals("kids")) {
			System.out.println("they can watch Pogo");
		}else {
			System.out.println("they can watch anything");
		}
	}
	
	public void whoCanVote(String s) {
		if(s.equals("kids")) {
			System.out.println("they cannot  vote");
		}else {
			System.out.println("they can vote");
		}
	}
	
	public static void main(String[] args) {

		MethodsInjava obj = new MethodsInjava();
//		obj.evenOdd(20);
//		obj.evenOdd(34);;
		
		String str = obj.calculateAge(15);
		
		obj.channels(str);
		obj.whoCanVote(str);
		
	}

}
