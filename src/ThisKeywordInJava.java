
public class ThisKeywordInJava {

	int a = 10; // instance variable
	
	public ThisKeywordInJava(){
		System.out.println("in default cons");
	}
	
	public ThisKeywordInJava(int b){
		this();
		System.out.println("in parame cons");
	}
	
	
	public void show() {
		
		this.a=50;	
		
	}

	
	public void display(int a) {
		
		this.a=a;
		System.out.println("in display");
	}
	public void print() {
		System.out.println(a);
	}
	
	public static void main(String[] args) {

		ThisKeywordInJava obj = new ThisKeywordInJava();
		
		obj.print();
		obj.show();
		obj.print();
		obj.display(100000);
		obj.print();

	}

}
