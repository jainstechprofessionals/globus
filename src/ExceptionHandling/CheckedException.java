package ExceptionHandling;

public class CheckedException {

	public void show(int a, int b) throws InterruptedException {
		Thread.sleep(1000);
		System.out.println("in show");
		System.out.println(a + b);
	}

	public static void main(String[] args) throws InterruptedException {
		CheckedException oj = new CheckedException();
		oj.show(10, 5);

	}

}
