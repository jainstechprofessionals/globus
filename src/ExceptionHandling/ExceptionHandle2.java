package ExceptionHandling;

public class ExceptionHandle2 {

	public static void main(String[] args) {
		int a = 10;
		int b = 10;
		String str = "null";
		int arr[] = new int[3];
		try {
			System.out.println(a / b);
			System.out.println(str.length());
			System.out.println(arr[0]);
			System.out.println(arr[3]);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Please enter a non zero value");
		}

		// exceptions
//		1. Checked
//		2. Unchecked 

		// try
		// catch
		// throw
		// throws
		// finally

	}

}
