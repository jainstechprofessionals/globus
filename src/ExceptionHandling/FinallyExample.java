package ExceptionHandling;

public class FinallyExample {

	public static void main(String[] args) {
		int a = 10;
		int b = 0;

		try {
			System.out.println(a / b);
			System.out.println("hello");
		} catch (Exception e) {
			System.out.println("in catch block");

		} finally {
			System.out.println("in finally block");
		}

	}

}
