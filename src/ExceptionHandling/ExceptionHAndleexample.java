package ExceptionHandling;

public class ExceptionHAndleexample {

	public static void main(String[] args) {

		int a = 10;
		int b = 0;
		String str = null;

		try {
			System.out.println(a / b);
			System.out.println(str.length());
			
		} catch (ArithmeticException e) {
			e.printStackTrace();
			System.out.println("Please enter a non zero value");
		}catch(NullPointerException altaf) {
			altaf.printStackTrace();
			System.out.println("String is null");
		}

		// exceptions
//		1. Checked
//		2. Unchecked 

		// try
		// catch
		// throw
		// throws
		// finally

	}

}
