package MultipleInheritanceInJava;

public class Rectangle implements Printable{

	@Override
	public void print() {
		System.out.println("in print of rectangle");
		
	}

	@Override
	public void draw() {
		System.out.println("in draw of Rectangle");
		
	}

	public void display() {
		System.out.println("in display");
	}
	
	
}
