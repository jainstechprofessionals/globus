package Inheritance;

public class SBI extends Bank {
	
	

	public int getRateOfInterest() {
		return 8;
	}

	
	public void display() {
		this.getRateOfInterest();
		super.getRateOfInterest();
		show();
	}
}

// super class , Parent class

// child class , sub class

// polymorphism 
// interface
//abstraction 
// exception handle
// collections
// encapsulation 
