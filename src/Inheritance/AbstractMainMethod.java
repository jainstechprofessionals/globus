package Inheritance;

public class AbstractMainMethod {
	public static void main(String[] args) {
		String str = "oriental";
		PolicyB obj = null;
		if (str.equalsIgnoreCase("hsbc")) {
			obj = new HSBC(); // run time poly, dynamic method dispatch, upcasting, late binding
		} else if (str.equalsIgnoreCase("oriental")) {
			obj = new Oriental();
		}
		obj.display();
		obj.getRateOfIntrest();
	}
}
