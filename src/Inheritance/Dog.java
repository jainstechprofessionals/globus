package Inheritance;

public class Dog extends Animal {
	int a = 90; // instance
	int b = 90;

	Dog() {
		super(10);
		System.out.println("in dog");	
	}	

	public static void show1() {
		System.out.println("asdf");
	}
	
	public void dogSound() {
//		int a =89;  // local variable 
//		System.out.println(a);
//		System.out.println(this.a);
//		System.out.println(super.a);
		System.out.println(" I bark");
	}

	public void show() {

	}
}
