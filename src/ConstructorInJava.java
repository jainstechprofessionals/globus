
public class ConstructorInJava {

	int a; // instance variable
	
	public ConstructorInJava() {
		a=2;
		System.out.println("in constructor");
	}

	public ConstructorInJava(int i) {
		a=i;
		System.out.println("in constructor");
	}

	
	public void show(int i) {
		
		
		System.out.println(a*i);
		
		System.out.println("in show");
		
	}

	public void display() {
	
		System.out.println("in display");
	}

	public static void ss() {
		System.out.println("in ss");
	
	}
	
	public static void main(String[] args) {

//		ConstructorInJava obj = new ConstructorInJava();
//
//		obj.show(10);
		
		
	
		ConstructorInJava obj1 = new ConstructorInJava(20);

		obj1.show(30);

	}

}
